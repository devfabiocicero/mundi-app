import {
  number, string, shape, oneOfType, array, arrayOf,
} from 'prop-types';

export const employeeType = shape({
  id: number.isRequired,
  name: string.isRequired,
  role: string.isRequired,
  email: string.isRequired,
  phone: shape({
    number: string.isRequired,
    ddd: string.isRequired,
  }),
  phone2: shape({
    number: string.isRequired,
    ddd: string.isRequired,
  }),
});

export const employeesType = oneOfType([
  array,
  arrayOf([
    employeeType,
  ]),
]);
