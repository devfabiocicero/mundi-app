const alphabet = [
  'A',
  'B',
  'C',
  'D',
  'E',
  'F',
  'G',
  'H',
  'I',
  'J',
  'K',
  'L',
  'M',
  'N',
  'O',
  'P',
  'Q',
  'R',
  'S',
  'T',
  'U',
  'V',
  'W',
  'X',
  'Y',
  'Z',
];

export const returnSiblingsLetters = (letter) => {
  const lettersList = alphabet.filter((_, index) => {
    const letterIndex = alphabet.indexOf(letter);

    return (
      letterIndex === index
      || letterIndex + 1 === index
      || letterIndex - 1 === index
    );
  });

  return lettersList;
};
