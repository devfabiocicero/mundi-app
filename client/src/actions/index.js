
import { FETCH_EMPLOYEES, FETCH_EMPLOYEE, SELECT_LETTER } from './types';

import employees from '../apis/employees';

export const fetchEmployees = () => async (dispatch) => {
  const response = await employees.get('/employees');

  dispatch({ type: FETCH_EMPLOYEES, payload: response.data });
};

export const fetchEmployee = (employeeId) => async (dispatch) => {
  const response = await employees.get(`/employees/${employeeId}`);

  dispatch({ type: FETCH_EMPLOYEE, payload: response.data });
};

export const selectLetter = (selectedLetter) => ({
  type: SELECT_LETTER,
  payload: selectedLetter,
});
