import _ from 'lodash';
import {
  FETCH_EMPLOYEES,
  FETCH_EMPLOYEE,
} from '../actions/types';

const employeeReducer = (state = {}, action) => {
  switch (action.type) {
    case FETCH_EMPLOYEES:
      return { ...state, ..._.mapKeys(action.payload, 'id') };

    case FETCH_EMPLOYEE:
      return { ...state, [action.payload.id]: action.payload };

    default:
      return state;
  }
};

export default employeeReducer;
