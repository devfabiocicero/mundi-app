import { SELECT_LETTER } from '../actions/types';

const initialState = {
  activeLetter: 'A',
};

const sliderReducer = (state = initialState, action) => {
  switch (action.type) {
    case SELECT_LETTER:
      return { ...initialState, activeLetter: action.payload };

    default: return state;
  }
};

export default sliderReducer;
