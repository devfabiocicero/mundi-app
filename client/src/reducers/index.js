import { combineReducers } from 'redux';
import employeeReducer from './employeeReducer';
import sliderReducer from './sliderReducer';

export default combineReducers({
  employees: employeeReducer,
  slider: sliderReducer,
});
