import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const ProfileContainer = styled.div`
    padding-top: 32px;
    display: flex;
    flex-direction: column;
    align-items: center;
`;

export const ArrowBack = styled.i`
    border: solid white;
    border-width: 0 3px 3px 0;
    display: inline-block;
    padding: 6px;

    transform: rotate(135deg);
    -webkit-transform: rotate(135deg);
`;

export const LinkBack = styled(Link)`
    align-self: flex-start;
    margin-left: 23px;
`;
