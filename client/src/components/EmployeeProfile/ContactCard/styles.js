import styled from 'styled-components';

export const ContactCardStyle = styled.div`
  background-color: #405364;
  padding: 57px 16px 20px;
  border-radius: 20px;
  margin: 0 20px 35px;
  color: #fff;
  text-align: center;

  & > img:last-child {
    width: 100%;
    margin-top: 32px;
  }
`;

export const ContactCardPhoneBox = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  height: 100%;
  text-align: left;
`;

export const ContactCardInnerBox = styled.div`
  display: flex;
  height: 62px;
  justify-content: center;
  align-items: center;
  margin-bottom: 24px;

  & > div:first-child {
    display: flex;
    border-right: 1px solid #fff;
    padding-right: 17px;
    margin-right: 12px;
    height: 100%;
  }
`;
