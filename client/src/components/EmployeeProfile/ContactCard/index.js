import React from 'react';
import { employeeType } from '../../../types/employeeType';
import {
  ContactCardStyle,
  ContactCardPhoneBox,
  ContactCardInnerBox,
} from './styles';
import MundialeLogo from '../../../assets/images/mundiale-logo.svg';
import DivisaoHorizontal from '../../../assets/images/divisao-horizontal.svg';

const ContactCard = ({ employee }) => (
  <div>
    {
      employee && (
      <ContactCardStyle>
        <ContactCardInnerBox>
          <div>
            <img src={MundialeLogo} alt="Logomarca da Mundiale" />
          </div>
          <ContactCardPhoneBox>
            <div>
              <span>{`${employee.phone.ddd} | ${employee.phone.number}`}</span>
            </div>
            <div>
              <span>{`${employee.phone2.ddd} | ${employee.phone2.number}`}</span>
            </div>
          </ContactCardPhoneBox>
        </ContactCardInnerBox>
        <span>{employee.email}</span>
        <img src={DivisaoHorizontal} alt="Barra de divisão horizontal" />
      </ContactCardStyle>
      )
    }
  </div>
);

ContactCard.propTypes = {
  employee: employeeType.isRequired,
};

export default ContactCard;
