
import React from 'react';
import { employeeType } from '../../../types/employeeType';
import { QRCodeContainer } from './styles';
import QRCodeImage from '../../../assets/images/qr-code.png';

const QRCode = ({ employee }) => (
  <div>
    {
      employee && (
      <QRCodeContainer>
        <img src={QRCodeImage} alt="QR Code" />
      </QRCodeContainer>
      )
    }
  </div>
);

QRCode.defaultProps = {
  employee: null,
};

QRCode.propTypes = {
  employee: employeeType,
};

export default QRCode;
