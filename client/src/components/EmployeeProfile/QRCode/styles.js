import styled from 'styled-components';

export const QRCodeContainer = styled.div`
  background-color: #fff;
  padding: 20px;
  border-radius: 10px;
  width: 180px;

  img {
    max-width: 100%;
  }
`;
