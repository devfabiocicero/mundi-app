import styled from 'styled-components';

export const ProfileHeaderStyle = styled.div`
  color: #fff;
  text-align: center;
  margin-bottom: 20px;

  h5 {
    margin: 20px 0 10px;
    font-size: 24px;
  }

  img {
    border-radius: 50%;
    width: 150px;
  }
`;
