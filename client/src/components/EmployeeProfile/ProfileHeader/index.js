import React from 'react';
import { employeeType } from '../../../types/employeeType';
import { ProfileHeaderStyle } from './styles';
import FotoFuncionario from '../../../assets/images/funcionario.jpg';

const ProfileHeader = ({ employee }) => (
  <div>
    {
      employee && (
      <ProfileHeaderStyle>
        <img src={FotoFuncionario} alt="Foto do funcionário" />
        <h5>{employee.name}</h5>
        <span>{employee.role}</span>
      </ProfileHeaderStyle>
      )
    }
  </div>
);

ProfileHeader.propTypes = {
  employee: employeeType.isRequired,
};

export default ProfileHeader;
