import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { employeeType } from '../../types/employeeType';

import { fetchEmployee } from '../../actions';
import { ProfileContainer, ArrowBack, LinkBack } from './styles';
import BackgroundThreeColors from '../BackgroundThreeColors';
import ProfileHeader from './ProfileHeader';
import ContactCard from './ContactCard';
import QRCode from './QRCode';

const EmployeeProfile = ({
  fetchEmployee: fetchEmployeeConnnect,
  employee,
  match,
}) => {
  useEffect(() => {
    fetchEmployeeConnnect(match.params.id);
  }, [fetchEmployeeConnnect, match.params.id]);

  return (
    <>
      <BackgroundThreeColors />
      <ProfileContainer>
        <LinkBack to="/">
          <ArrowBack />
        </LinkBack>
        <ProfileHeader employee={employee} />
        <ContactCard employee={employee} />
        <QRCode employee={employee} />
      </ProfileContainer>
    </>
  );
};

EmployeeProfile.propTypes = {
  fetchEmployee: PropTypes.func.isRequired,
  employee: employeeType.isRequired,
  match: PropTypes.shape({
    params: PropTypes.object,
  }).isRequired,
};

const mapStateToProps = (state, ownProps) => ({
  employee: state.employees[ownProps.match.params.id],
});

export default connect(mapStateToProps, { fetchEmployee })(EmployeeProfile);
