
import styled from 'styled-components';

export const ThreeColorsStyle = styled.div`
  position: fixed;
  width: inherit;
  z-index: -1;

  div {
    height: 29.4vh;
    background-color: #405364;
  }

  div:first-child {
    height: 13.4vh;
    background-color: #84daf1;
  }

  div:nth-child(2) {
    height: 57.3vh;
    background-color: #0fb3d8;
  }
`;
