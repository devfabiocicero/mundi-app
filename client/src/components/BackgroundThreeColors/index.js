
import React from 'react';
import GradientBackground from '../GradientBackground';
import { ThreeColorsStyle } from './styles';

const BackgroundThreeColors = () => (
  <ThreeColorsStyle>
    <div />
    <GradientBackground />
    <div />
  </ThreeColorsStyle>
);

export default BackgroundThreeColors;
