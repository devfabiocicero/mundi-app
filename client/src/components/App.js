import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import EmployeeList from './EmployeeList';
import EmployeeProfile from './EmployeeProfile';

import { GlobalStyle } from './globalStyle';
import { Container } from './styles';

const App = () => (
  <>
    <BrowserRouter>
      <GlobalStyle />
      <Container>
        <Route path="/" exact component={EmployeeList} />
        <Route path="/funcionarios/perfil/:id" exact component={EmployeeProfile} />
      </Container>
    </BrowserRouter>
  </>
);

export default App;
