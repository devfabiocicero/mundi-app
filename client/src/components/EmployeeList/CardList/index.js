
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { employeesType } from '../../../types/employeeType';
import { fetchEmployees } from '../../../actions';
import Card from '../Card';

const CardList = ({ fetchEmployees: fetchEmployeesConnect, employees }) => {
  useEffect(() => {
    fetchEmployeesConnect();
  }, [fetchEmployeesConnect]);

  return (
    <div>
      {employees.map((employee) => (
        <Card key={employee.id} employee={employee} />
      ))}
    </div>
  );
};

CardList.propTypes = {
  employees: employeesType.isRequired,
  fetchEmployees: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  const { activeLetter } = state.slider;
  const employees = Object.values(state.employees);
  return {
    employees: employees.filter((employee) => employee.name.startsWith(activeLetter)),
  };
};

export default connect(mapStateToProps, { fetchEmployees })(CardList);
