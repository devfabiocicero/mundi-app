
import React from 'react';
import { employeeType } from '../../../types/employeeType';
import { LinkStyle, CardStyle, CardInnerBox } from './style';
import FotoFuncionario from '../../../assets/images/funcionario.jpg';

const Card = ({ employee }) => (
  <LinkStyle to={`/funcionarios/perfil/${employee.id}`}>
    <CardStyle>
      <div>
        <>
          <img src={FotoFuncionario} alt="Foto do funcionário" />
          <CardInnerBox>
            <div>
              <h6>{employee.name}</h6>
              <span>{employee.role}</span>
            </div>
          </CardInnerBox>
        </>
      </div>
    </CardStyle>
  </LinkStyle>
);

Card.propTypes = {
  employee: employeeType.isRequired,
};

export default Card;
