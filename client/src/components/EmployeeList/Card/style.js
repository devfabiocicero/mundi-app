import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const LinkStyle = styled(Link)`
  text-decoration: none;
`;

export const CardStyle = styled.div`
         position: relative;

         & > div {
           display: flex;
           align-items: center;
           border-radius: 20px;
           background: #fff;
           width: 70%;
           margin: 0 auto;
           height: 80px;
           margin-bottom: 25px;
           overflow: hidden;
           color: #929292;
           -webkit-box-shadow: 7px 7px 7px -4px rgba(0, 0, 0, 0.37);
           -moz-box-shadow: 7px 7px 7px -4px rgba(0, 0, 0, 0.37);
           box-shadow: 7px 7px 7px -4px rgba(0, 0, 0, 0.37);
         }

         & > div::before {
           content: "";
           display: block;
           border-radius: 50%;
           width: 12px;
           height: 12px;
           background-color: #7fcfd8;
           position: absolute;
           left: 25px;
           opacity: 0.7;
         }

         & > div::after {
           content: "+";
           font-weight: bold;
           font-size: 23px;
           color: #b1b1b1;
           margin-left: 15px;
         }

         img {
           max-height: 100%;
           margin-right: 12px;
         }
       `;

export const CardInnerBox = styled.div`
  display: flex;
  height: 78%;

  h6 {
    font-weight: bold;
    font-size: 20px;
    margin: 10px 0 5px;
  }

  & > div {
    width: 168px;
    border-right: 1px solid #bfbfbf;
  }
`;
