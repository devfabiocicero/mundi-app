
import React from 'react';
import Slider from './Slider';
import CardList from './CardList';
import GradientBackground from '../GradientBackground';

const EmployeeList = () => (
  <>
    <GradientBackground position="fixed" />
    <Slider />
    <CardList />
  </>
);

export default EmployeeList;
