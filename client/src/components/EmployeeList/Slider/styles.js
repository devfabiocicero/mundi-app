import styled from 'styled-components';

export const SliderStyles = styled.div`
  position: relative;
  margin-bottom: 28px;

  span {
    cursor: pointer;
    user-select: none;
    outline: none;
  }

  & > div {
    position: absolute;
    top: 56%;
    left: 26%;
    font-size: 35px;
    letter-spacing: 72px;
    -webkit-letter-spacing: 72px;
    -moz-letter-spacing: 72px;
    -ms-letter-spacing: 72px;
    color: #fff;
    font-family: sans-serif;
  }

  img {
    width: 100%;
  }
`;
