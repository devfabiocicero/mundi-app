
import React, { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { selectLetter } from '../../../actions';
import { SliderStyles } from './styles';
import { returnSiblingsLetters } from '../../../helpers/alphabet';
import MundialeVectorSlider from '../../../assets/images/mundiale-vector-slider.svg';

const Slider = ({ selectLetter: selectedLetterConnect }) => {
  const [pagination, setPagination] = useState(['A', 'B', 'C']);

  const onSelectLetter = (selectedLetter) => {
    const siblingsLetters = returnSiblingsLetters(selectedLetter);

    setPagination(siblingsLetters);
    selectedLetterConnect(selectedLetter);
  };

  return (
    <SliderStyles>
      <div>
        {pagination.map((letter) => (
          <span
            key={uuidv4()}
            onClick={() => onSelectLetter(letter)}
            onKeyPress={() => onSelectLetter(letter)}
            role="button"
            tabIndex="0"
          >
            {letter}
          </span>
        ))}
      </div>
      <img src={MundialeVectorSlider} alt="Vetor de slider da Mundiale" />
    </SliderStyles>
  );
};

Slider.propTypes = {
  selectLetter: PropTypes.func.isRequired,
};

export default connect(null, { selectLetter })(Slider);
