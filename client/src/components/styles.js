import styled from 'styled-components';

export const Container = styled.div`
    width: 440px;
    margin: 0 auto;
    font-family: "Times New Roman", Times, serif;
`;
