import styled from 'styled-components';

export const GradientBackgroundStyle = styled.div`
  background: #b3dced;
  background: -moz-linear-gradient(
    top,
    #b3dced 0%,
    #03b4e6 0%,
    #03b4e6 16%,
    #44b389 100%
  ); /* FF3.6-15 */
  background: -webkit-linear-gradient(
    top,
    #b3dced 0%,
    #03b4e6 0%,
    #03b4e6 16%,
    #44b389 100%
  ); /* Chrome10-25,Safari5.1-6 */
  background: linear-gradient(
    to bottom,
    #b3dced 0%,
    #03b4e6 0%,
    #03b4e6 16%,
    #44b389 100%
  ); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b3dced', endColorstr='#44b389',GradientType=0 );

  position: ${(props) => props.position};
  height: 100vh;
  width: inherit;
  z-index: -1;
`;
