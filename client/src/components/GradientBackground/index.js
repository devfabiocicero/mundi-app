
import React from 'react';
import PropTypes from 'prop-types';
import { GradientBackgroundStyle } from './styles';

const GradientBackground = ({ position }) => (
  <GradientBackgroundStyle position={position} />
);

GradientBackground.defaultProps = {
  position: '',
};

GradientBackground.propTypes = {
  position: PropTypes.string,
};

export default GradientBackground;
